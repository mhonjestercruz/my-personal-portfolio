// declaration of 3 variables for my toggling mobile menu
const menu = document.querySelector('#mobile__menu');
const menuLinks = document.querySelector('.navbar__menu');
const navbarLogo = document.querySelector('#navbar__logo');

// function for toggling mobile menu
const myToggleBar = ()  => {
    // to toggle the bars in mobile menu
    menu.classList.toggle('is-active');
    // to toggle the navbar menu
    menuLinks.classList.toggle('active');
}
// add event listener to activate the toggler inside the function
menu.addEventListener('click', myToggleBar);


// function to show the active menu when scrolling
const highlightMenu = () => {
    // declaration of 5 variables for navbar highlights
    const navHighlighted = document.querySelector('.highlight');
    const navHome = document.querySelector('#home__page');
    const navAbout = document.querySelector('#about__page');
    const navPortfolio = document.querySelector('#portfolio__page');
    const navContact = document.querySelector('#contact__page');

    // declaration for windows scrolling horizontally
    let scrollPosition = window.scrollY;
    console.log(scrollPosition)
    // logical statement for highlight effect

    // if my windows inner width is lessthan 960px and height is less than 500px
    if ( window.innerWidth > 960 && scrollPosition < 500 ) {
        // add the class property of highlight to navigation home
        navHome.classList.add('highlight');
        // and remove the class property of highlight navigation about
        navAbout.classList.remove('highlight');
        // then return the result
        return
    // if my windows inner width is less then 960px and height is less than 1000px
    } else if ( window.innerWidth > 960 && scrollPosition < 1000) {
        // remove the class property of highlight to navigation home
        navHome.classList.remove('highlight');
        // then add the class property of highlight to navigation abut
        navAbout.classList.add('highlight');
        // then remove the class property of highlight to navigation portfolio
        navPortfolio.classList.remove('highlight');
        // return the result
        return
    // if my windows inner width is lessthan 960px and height is less than 1500px
    } else if ( window.innerWidth > 960 && scrollPosition < 1500) {
        // remove the class property of highlight to navigation about
        navAbout.classList.remove('highlight');
        // then add the class property of highlight to navigation portfolio
        navPortfolio.classList.add('highlight');
        // then remove the class property of highlight to navigation contact
        navContact.classList.remove('highlight');
        // return the result
        return
    // if my windows inner width is lessthan 960px and height is less than 2027px
    } else if ( window.innerWidth > 960 && scrollPosition < 1800) {
        // remove the class property of highlight to navigation portfolio
        navPortfolio.classList.remove('highlight');
        // then add the class property of highlight to navigation contact
        navContact.classList.add('highlight');
        // return the result
        return
    }
    // if the inner width of the window and the class highlight is lessthan 960px and scroll position is lessthan 600px or class highlight shrink then remove the highlight class
    if ( navHighlighted && window.innerWidth < 960 || navHighlighted ) {
        navHighlighted.classList.remove('highlight');
    }
    // if statement for the active navbar
    // if the window inner width is less than 960px and the height is less than 323px
    if ( window.innerWidth < 960 && scrollPosition < 323 ) {
        // then i want the home navigation style to be opacity = 1
        document.getElementById('home__page').style.opacity = '1';
        // and i want the about navigation style to be opacity = 0.5
        document.getElementById('about__page').style.opacity = '0.5';
        // return the result
        return
    // if the window inner width is less than 960px and the height is less than 872px but more than 323px
    } else if ( window.innerWidth < 960 && scrollPosition < 872 && scrollPosition > 323 ) {
        // then i want the home navigation style to be opacity = 0.5px
        document.getElementById('home__page').style.opacity = '0.5';
        // and i want the about navigation style to be opacity = 1px
        document.getElementById('about__page').style.opacity = '1';
        // and i want the portfolio navigation style to be opacity = 0.5px
        document.getElementById('portfolio__page').style.opacity = '0.5';
        // return the result
        return
    // if the window inner width is less than 960px and the height is less than 1500px but more than 872px
    } else if ( window.innerWidth < 960 && scrollPosition < 1500 && scrollPosition > 872 ) {
        // then i want the about navigation style to be opacity = 0.5px
        document.getElementById('about__page').style.opacity = 0.5;
        // and i want the portfolio navigation style to be opacity = 1px
        document.getElementById('portfolio__page').style.opacity = '1';
        // and i want the contact navigation style to be opacity = 0.5px
        document.getElementById('contact__page').style.opacity = '0.5';
        // return the result
        return
    // if the window inner width is less than 960px and the height is less than 1653px but more than 1500px
    }  else if ( window.innerWidth < 960 && scrollPosition < 1653 && scrollPosition > 1500 ) {
        // then i want the portfolio navigation style to be opacity = 0.5px
        document.getElementById('portfolio__page').style.opacity = 0.5;
        // and i want the contact navigation style to be opacity = 1px
        document.getElementById('contact__page').style.opacity = '1';
        // return the result
        return
    // if the window inner width is less than 960px and the height is more than 1653px
    } else if ( window.innerWidth < 960 && scrollPosition > 1653 ) {
        // then i want the contact navigation style to be opacity = 0.5px
        document.getElementById('contact__page').style.opacity = '0.5'
        // return the result
        return
    }
}

// add a scroll event listener to the window using the function of highlightMenu
window.addEventListener('scroll', highlightMenu);
// add a click event listener to the window using the function of highlight
window.addEventListener('click', highlightMenu);
// add a window onload to load the function right away
window.onload = () => highlightMenu();


// function for closing the mobile menu when some of the links are clicked
const hideMobileMenu = () => {
    // declaring variable to get the toggle bar for mobile menu
    const menuBars = document.querySelector('.is-active');
    // if the inner width of window is less then or equal to 768 pixels and menu bar shrink
    if ( window.innerWidth <= 768 && menuBars) {
        // i want to toggle the bar to close the class list active
        menu.classList.toggle('is-active');
        menuLinks.classList.remove('active');
    }
}

// add event listeners to the toggle bar and logo to close the menu bar when clicked
menuLinks.addEventListener('click', hideMobileMenu);
navbarLogo.addEventListener('click', hideMobileMenu);


