// declaration of 3 variables for my toggling mobile menu
const menu = document.querySelector('#mobile__menu');
const menuLinks = document.querySelector('.navbar__menu');
const navbarLogo = document.querySelector('#navbar__logo');
// declaration of 3 variables for toggling project filters
let filterContainer = document.getElementById('portfolio__nav');
let filterButtons = filterContainer.getElementsByClassName('portfolio__filter');




// function for toggling mobile menu
const myToggleBar = ()  => {
    // to toggle the bars in mobile menu
    menu.classList.toggle('is-active');
    // to toggle the navbar menu
    menuLinks.classList.toggle('active');
}
// add event listener to activate the toggler inside the function
menu.addEventListener('click', myToggleBar);

// function for closing the mobile menu when some of the links are clicked
const hideMobileMenu = () => {
    // declaring variable to get the toggle bar for mobile menu
    const menuBars = document.querySelector('.is-active');
    // if the inner width of window is less then or equal to 768 pixels and menu bar shrink
    if ( window.innerWidth <= 768 && menuBars) {
        // i want to toggle the bar to close the class list active
        menu.classList.toggle('is-active');
        menuLinks.classList.remove('active');
    }
}

// add event listeners to the toggle bar and logo to close the menu bar when clicked
menuLinks.addEventListener('click', hideMobileMenu);
navbarLogo.addEventListener('click', hideMobileMenu);


// Add toggler for active filter buttons
for ( let eachFilter = 0; eachFilter < filterButtons.length; eachFilter++ ) {
    filterButtons[eachFilter].addEventListener('click', function() {
        let myActive = document.getElementsByClassName('is__active')
        myActive[0].className = myActive[0].className.replace(' is__active', '');
        this.className += ' is__active';
    });
}


filterSelection('new__Projects');

function filterSelection(c) {
    // get the container of each image or projects
    let x = document.getElementsByClassName('project');
    // if the parameter is equal to all then the projects must show all
    if ( c == 'all' ) c = '';
    // to add the 'show__projects' class (display block) to the filtered elements, and remove the class 'show__projects' from the unselected elements.
    for ( let i = 0; i < x.length; i++ ) {
        removeElements(x[i], 'show__projects');
        if ( x[i].className.indexOf(c) > -1 ) addElements(x[i], 'show__projects')
    }
}

// show the filtered elements
function addElements(elements, name) {
    let array1 = elements.className.split(' ');
    let array2 = name.split(' ');
    for ( let i = 0; i < array2.length; i++ ) {
        if ( array1.indexOf(array2[i]) == -1 ) {
            elements.className += ' ' + array2[i];
        }
    }
}


// hide the unselected elements
function removeElements(elements, name) {
    let array1 = elements.className.split(' ');
    let array2 = name.split(' ');
    for ( let i = 0; i < array2.length; i++ ) {
        while( array1.indexOf(array2[i]) > -1 ){
            array1.splice(array1.indexOf(array2[i]), 1);
        }
    }
    elements.className = array1.join(' ');
}