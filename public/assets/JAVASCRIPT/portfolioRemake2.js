// declaration of 3 variables for my toggling mobile menu
const menu = document.querySelector('#mobile__menu');
const menuLinks = document.querySelector('.navbar__menu');
const navbarLogo = document.querySelector('#navbar__logo');


// function for toggling mobile menu
const myToggleBar = ()  => {
    // to toggle the bars in mobile menu
    menu.classList.toggle('is-active');
    // to toggle the navbar menu
    menuLinks.classList.toggle('active');
}
// add event listener to activate the toggler inside the function
menu.addEventListener('click', myToggleBar);

// function for closing the mobile menu when some of the links are clicked
const hideMobileMenu = () => {
    // declaring variable to get the toggle bar for mobile menu
    const menuBars = document.querySelector('.is-active');
    // if the inner width of window is less then or equal to 768 pixels and menu bar shrink
    if ( window.innerWidth <= 768 && menuBars) {
        // i want to toggle the bar to close the class list active
        menu.classList.toggle('is-active');
        menuLinks.classList.remove('active');
    }
}

// add event listeners to the toggle bar and logo to close the menu bar when clicked
menuLinks.addEventListener('click', hideMobileMenu);
navbarLogo.addEventListener('click', hideMobileMenu);

// to remove white space on textarea
removeTextAreaWhiteSpace()
function removeTextAreaWhiteSpace() {
    let myTxtArea = document.getElementById('message');
    // means replace the whitespace with none
    myTxtArea.value = myTxtArea.value.replace(/^\s*|\s*$/g, '');
}
